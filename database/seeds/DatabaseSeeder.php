<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(\App\Model\Master\Pengguna::class, 10)->create();
        $cities = factory(\App\city::class, 10)->create();
    }
}
