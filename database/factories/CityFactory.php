<?php

use Faker\Generator as Faker;

$factory->define(App\city::class, function (Faker $faker) {
    return [
        'nama' => $faker->city,
        'kodepos' => $faker->postcode
    ];
});
