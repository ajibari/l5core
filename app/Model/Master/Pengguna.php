<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Trackable;
use Hashids;

class Pengguna extends Model
{
    use Notifiable;
	use Trackable;

    protected $table = 'pengguna';
    protected $primaryKey = 'id_pengguna'; //nama primary key harus unik, bukan id aja

    protected $fillable = ['nama', 'email', 'password'];
    protected $hidden = ['id_pengguna'];
    protected $foreignKeys = [];
    protected $appends = ['id'];
    
    public function getIdAttribute()
    {
        $keys = is_null($this->getForeignKeys())?[]:$this->getForeignKeys();
        $primaryKey = $this->getKeyName();
        array_push($keys, $primaryKey);

        $salt = env("SALT_KEY");
        $hashids = new Hashids\Hashids($salt);

        foreach($keys as $f => $v){
            $unhashed[$v] = $hashids->encode($this->getAttributeValue($v));
        }

        return $unhashed;

    }
}