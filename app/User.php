<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Trackable;
use Hashids;

class User extends Authenticatable
{
    use Notifiable;
    use Trackable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getIdAttribute()
    {
        $keys = is_null($this->getForeignKeys())?[]:$this->getForeignKeys();
        $primaryKey = $this->getKeyName();
        array_push($keys, $primaryKey);

        $salt = env("SALT_KEY");
        $hashids = new Hashids\Hashids($salt);

        foreach($keys as $f => $v){
            $unhashed[$v] = $hashids->encode($this->getAttributeValue($v));
        }

        return $unhashed;

    }
}
