<?php
//app/Helpers/Envato/User.php
namespace App\Helper\Integra;
 
use Illuminate\Support\Facades\DB;
 
class General {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */

	public static function fooBar() 
	   {
	      return "FoOOBAR";
	   }

	
	public static function renderResponse() 
	{
	  	return \App\Helper\Integra\CustomHelper::renderResponse();
	}

	public static function saveMongo()
	{
		return \App\Helper\Integra\CustomHelper::saveMongo();
	}

	public static function getMongo()
	{
		return \App\Helper\Integra\CustomHelper::getMongo();
	}
}