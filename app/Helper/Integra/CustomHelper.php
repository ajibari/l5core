<?php

use MongoDB\Client as Mongo;

function getUser()
{
	$result = \App\Model\Master::where('id', '=', 1)
	    ->first()->toJson();

	return $result;
}


function renderResponse($result, $success, $message){
    $output = array();
    $output['Result'] = $result;
    $output['Success'] = $success;
    $output['Message'] = $message;
    return response()->json($output);
}

function fooBar(){
    echo "foobar";
}

function saveMongo($data, $dbMongo, $collection){

    $collections = \Mongo::get()->$dbMongo->$collection;

    return $collections->insertOne($data);
}

function getMongo($cond, $dbMongo, $collection){
	
    $collections = \Mongo::get()->$dbMongo->$collection;

	return json_encode($collections->find($cond)->toArray());
}

function updateMongo(){

}

function delMongo(){

}