<?php 

namespace App\Traits;

use App\Model\Master\User;
use Illuminate\Support\Facades\DB;
use MongoDB\Client as Mongo;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Hashids;

trait Trackable
{
    
    public static function bootTrackable()
    {
        
        static::retrieved(function ($model) {           

        });

        static::creating(function ($model) {

            $request = request();
            $input = $request->header();

            if (isset($input['token'])){
                $auth = User::where('remember_token', $input['token'])->first();
                $ip_address = $request->ip();
                
                if(!is_null($auth)){
                    $model->created_by = $auth->id;
                    $model->alamat_ip = $ip_address;    
                }
                
            }

            static::saveQuery($input, $request, $model);

        });

        static::updating(function ($model) {

            $request = request();
            $input = $request->header();

            if (isset($input['token'])){
                $auth = User::where('remember_token', $input['token'])->first();
                $ip_address = $request->ip();
                
                if(!is_null($auth)){
                    $model->updated_by = $auth->id;
                    $model->alamat_ip = $ip_address;
                }

                static::saveQuery($input, $request, $model);
            }
        });

        static::deleting(function ($model) {
            
            $request = request();
            $input = $request->header();

            static::saveQuery($input, $request, $model);

        });
    }

    public static function saveQuery($input, $request, $model)
    {
        if(!empty($input['token'])){
            $user = User::where('remember_token', $input['token'])->first();
            $ip_address = $request->ip();

            DB::listen(function ($query) use ($user, $ip_address) {
                $query_action = explode(' ',trim($query->sql));
                $conditions = ['update','insert','delete'];

                if(in_array($query_action[0], $conditions)){
                    if(!empty($user->id)){

                        if(env("APP_DEBUG") == true){
                            //insert into laravel log
                            Log::info($query->sql . ' - ' . serialize($query->bindings) . ' >>> '.$query->time.' ms >>> user_id:'.$user->id);
                        }

                        $log_data = ['user_id' => $user->id, 'action' => $query->sql.' - '.serialize($query->bindings).' exec_time '.$query->time, 'date' => date('Y-m-d H:i:s'), 'ip_address' => $ip_address];

                        $DbMongo = 'DB_'.date('Y_m_d');
                        $DbCollections = 'Query';
                        $collection = \Mongo::get()->$DbMongo->$DbCollections;

                        return $collection->insertOne($log_data);
                    }
                }
            });
        }
    }

    public function getForeignKeys(){
        return $this->foreignKeys;
    }

    public function encodeForeignKeys($model, $input, $method = null){
        
        $salt = env("SALT_KEY");
        $hashids = new Hashids\Hashids($salt);

        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $primaryKey = $model->getKeyName();
        $result = [];
        $result['input'] = [];
        $result['unhashed'] = [];

        $result['input'] = array_merge($result['input'], $input);

        foreach($foreignKeys as $f => $v){
            $id = $hashids->decode($input[$v]);
            if(!is_null($id[0])){
                $unhashed[$v] = $id[0];
                unset($input[$v]);
            }
        }

        //unhashed primary key except for create and update method
        if(is_null($method)){
            $id = $hashids->decode($input[$primaryKey]);
            unset($input[$primaryKey]);
            if(!is_null($id[0])){
                $unhashed[$primaryKey] = $id[0];
            }
        }

        array_push($result['unhashed'], $unhashed);
        $result['unhashed'] = array_merge($result['unhashed'][0], $input);
        
        return $result;
    }

}