<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('master/pengguna',['uses' => 'Master\UserController@get_all']);
Route::get('master/pengguna/{id}',['uses' => 'Master\UserController@get']);
Route::get('master/pengguna/get_by/{param}/{value}',['uses' => 'Master\UserController@get_by']);
// Route::get('master/pengguna/{start}/{limit}',['uses' => 'Master\UserController@get_limit']);
Route::get('master/pengguna/create/s',['uses' => 'Master\UserController@create']);
Route::put('master/pengguna',['uses' => 'Master\UserController@update']);
Route::delete('master/pengguna',['uses' => 'Master\UserController@del']);